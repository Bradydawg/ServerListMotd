# ServerListMOTD Changelog v1.2.6
**This update for ServerListMOTD brings a few new features, and fully adds support for some old ones, so let's dive right in!**

## Added Features/Updates:
- Ability to customize server icon path and filename (Config regeneration **is** required if you want this feature, if you don't really care, no regeneration is required.)
- The auto update feature has been fully fixed. For some reason the spigot API was reporting incorrect numbers to the plugin, but we have solved the issue!

## Quick Reminder of old features:
- The API version is currently set to 1.13, and we are going to keep it set to 1.13 for as long as we can. This basically means that you can run our plugin on Spigot 1.13+

If you have any troubles or would like to let us know of bugs, issues can be created here: https://github.com/Bradydawg/ServerListMotd/issues

## Have a few features to suggest?
Feel free to fill out this quick form! (No sign-in required!)
https://forms.gle/87MXV3kxQrYLV4Vw5
